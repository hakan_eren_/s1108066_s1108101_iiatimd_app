package com.example.s1108066_s1108101_iiatimd_app;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Training.class}, version = 12)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TrainingDAO trainingDAO();

    private static AppDatabase instance;

    static synchronized AppDatabase getInstance(Context context){
        if (instance == null){
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context){
        return Room.databaseBuilder(context, AppDatabase.class, "trainingen").allowMainThreadQueries().fallbackToDestructiveMigration().build();
    }
}
