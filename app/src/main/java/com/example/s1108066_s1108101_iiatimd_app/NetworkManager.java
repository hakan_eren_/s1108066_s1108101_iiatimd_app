package com.example.s1108066_s1108101_iiatimd_app;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class NetworkManager extends ExcerciseActivity {
    //singelton
    private static final String TAG = "NetworkManager";
    private static final String WORKOUT_NAME = "workout.txt";
    private static NetworkManager instance = null;

    public RequestQueue requestQueue;

    private NetworkManager(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized NetworkManager getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkManager(context);
        }
        return instance;
    }

    public static synchronized NetworkManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Hij bestaat niet");
        }
        return instance;
    }

    public void getRequest(String prefixUrl, final VolleyCallBack volleyCallBack) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, prefixUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyCallBack.onSucces(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("fout", "Er gaat iets fout");
                Log.e("Volly Error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Log.d("fout", "Er gaat iets fout mapstring");
                Map<String, String> headers = new HashMap<>();
                String auth = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNWExZjVmNGI4MjFjNTFhZDg5M2VhZjhkMzkyYWU0ZGQ4ZTU4OTZjMGE0ZjZhYWMxYmIyMGIzZjZmNzRhZjFjYWM5ODk1ODFhZmE3ZmNhZmUiLCJpYXQiOjE1OTgxMDA0MzUsIm5iZiI6MTU5ODEwMDQzNSwiZXhwIjoxNjI5NjM2NDM1LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.8VPWwp_M5tZkaV9g-eNxOSkGl_mGC2KtJZdNrGgn4LqhMkehYL1HmJuwSkwHGsRF7XKf4IlA0DLx2ywlWAAVDnskd_M7OkZxeVprreex3xgjKmX5FvAsFBAIYEsYRdCWYMGoILX_ykHEGm-gusU18-olj-kEfoQqmfbXFJRrcSY8o55orB0twVAJhFbdRYix-aKZrLLj--BuvAJLpnr1Z1h7fl55kC2hpZ5gEn2iV83PTZoOhK9GMkLn_19WsMCgRP7g0xuz6VFY60pcYE58K88tPperIqr4RaX4Ev-qm2hnDVaarPGp38dPeqI26ZmJHklVQuB9MvXOHw9Mw6DTCh05P3DlMJdM5RQPV4hYYX6cK6OgXL3qZaqEcxq1lGp5eHunBN8_hBs-3c3Cz0szw3cszr-AvLlBR_7UOjsnlTD51zKWLpNg5Oha2m1Clxsy9CsjO5PjY3ampp7hsQNBPtUsU1xldqMdEW0mmlUueUZ6myhwhm6SNC7DHOFQqH3zMdvJa_m7T_DFGeuQVvX1FjacmX6mHpqbSNFtXgLu9jNMYtKc96k6zi541GHda_w9sdOoqG_VzG2udh3DB5_Y4369I-LhCKDZpcKiKFFhBiiJxQHgyqL6cYmz4_nUzz3SnXOK2IitSFxTExc9mrfVQPukvIV-_o3xW8Tkk7JLhCA";
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        requestQueue.add(stringRequest);
    }
}
