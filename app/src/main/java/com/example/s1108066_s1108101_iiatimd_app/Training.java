package com.example.s1108066_s1108101_iiatimd_app;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Training {

    @ColumnInfo
    private String name;
    @ColumnInfo
    private String calorie;

    @PrimaryKey
    private int uuid;

    public Training(String name,String calorie,int uuid){
        this.name = name;
        this.calorie = calorie;
        this.uuid = uuid;
    }

    public String getName(){
        return this.name;
    }
    public String getCalorie(){
        return this.calorie;
    }
    public int getUuid(){
        return this.uuid;
    }
}
