package com.example.s1108066_s1108101_iiatimd_app;

public class InsertTrainingTask implements Runnable {

    AppDatabase db;
    Training training;


    public InsertTrainingTask(AppDatabase db, Training training) {
        this.db = db;
        this.training = training;
    }

    @Override
    public void run() {
        db.trainingDAO().InsertTraining(this.training);
    }
}
