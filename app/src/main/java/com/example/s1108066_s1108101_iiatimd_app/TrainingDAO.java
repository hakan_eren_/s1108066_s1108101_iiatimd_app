package com.example.s1108066_s1108101_iiatimd_app;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TrainingDAO {

    @Query("SELECT * FROM Training")
    List<Training> getAll();

    @Insert
    void InsertTraining(Training training);



    @Delete
    void delete(Training training);
}
