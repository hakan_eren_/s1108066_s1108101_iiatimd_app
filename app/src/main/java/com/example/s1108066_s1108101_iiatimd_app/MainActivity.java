package com.example.s1108066_s1108101_iiatimd_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static android.graphics.Color.parseColor;

public class MainActivity extends AppCompatActivity {

    private Button exerciseButton;
    private Button postWButton;
    private Button homeButton;

    private static final String TRAINING_FILE = "training.txt";
    private static final String ACCOUNT_FILE = "account.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if (!file_checker()) {
                openSplashScreenActivity();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        homeButton = (Button) findViewById(R.id.home_button);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHomeActivity();
            }
        });

        exerciseButton = (Button) findViewById(R.id.exercise_button);
        exerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openExerciseActivity();
            }
        });

        postWButton = (Button) findViewById(R.id.post_workout_button);
        postWButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPostWorkoutActivity();
            }
        });



    }

    public void openHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.transitie_rechts_erin, R.anim.transitie_links_eruit);

    }

    public void openSplashScreenActivity() {
        Intent intent = new Intent(this, splashScreenActivity.class);
        startActivity(intent);
    }


    public void openExerciseActivity() {
        Intent intent = new Intent(this, ExcerciseActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.transitie_rechts_erin, R.anim.transitie_links_eruit);
    }


    public void openPostWorkoutActivity() {
        Intent intent = new Intent(this, PostworkoutActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.transitie_rechts_erin, R.anim.transitie_links_eruit);
    }

    public void openMainAtivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.transitie_rechts_erin, R.anim.transitie_links_eruit);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean file_checker() throws IOException {

        FileInputStream fis = null;
        boolean file_status = false;
        try {
            fis = openFileInput(ACCOUNT_FILE);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder waarde = new StringBuilder();
            String text;


            if ((text = br.readLine()) != null) {
                System.out.println("Text file is niet leeg");
                file_status = true;
            } else {
                System.out.println("Text file is leeg");
                file_status = false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file_status;
    }
}
