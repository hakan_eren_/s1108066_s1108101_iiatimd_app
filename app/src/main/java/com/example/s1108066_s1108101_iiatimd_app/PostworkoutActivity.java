package com.example.s1108066_s1108101_iiatimd_app;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.GridLayout.LayoutParams;
import android.widget.Toast;

import com.example.s1108066_s1108101_iiatimd_app.R;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static android.graphics.Color.parseColor;

public class PostworkoutActivity extends AppCompatActivity {

    private static final String FILE_NAME = "training.txt";
    private String waardes[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postworkout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        load();
    }

    public void reset(View v){
        FileOutputStream fos;
        try {
            String text = "";
            //Verander hier voor account.txt reset
            fos = openFileOutput("workout.txt" , MODE_PRIVATE);
            fos.write(text.getBytes());
            String text1 = "";
            //Verander hier voor account.txt reset
            fos = openFileOutput("training.txt" , MODE_PRIVATE);
            fos.write(text1.getBytes());
            String text2 = "";
            //Verander hier voor account.txt reset
            fos = openFileOutput("account.txt" , MODE_PRIVATE);
            fos.write(text2.getBytes());
            Toast.makeText(this, "Uw trainingen zijn gereset", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //laden van de opgeslagen data en plaatsen in de juiste plekken
    public void load() {
        FileInputStream fis = null;
        LinearLayout dynamic_view_training = (LinearLayout) findViewById(R.id.dynamic_layout_linear); //Linear voor de trainingen
        LinearLayout dynamic_view_training_aantal = (LinearLayout) findViewById(R.id.dynamic_layout_linear_aantal); //linear voor de aantallen
        LinearLayout dynamic_view_calorie = (LinearLayout) findViewById(R.id.dynamic_layout_linear2); // linear voor de calorieen
        int calorie = 0;

        try {
            fis = openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder waarde = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null) {
                waarde.append(text).append("\n");
            }

            System.out.println("Waarde" + waarde);
            String[] waardes = waarde.toString().split(",");
            if (waardes.length >= 3) {
                for (int i = 0; i < waardes.length - 1; i++) {
                    final TextView trainTextView = new TextView(this);
                    final TextView aantalTextView = new TextView(this);
                    final TextView calTextView = new TextView(this);
                    final TextView spaceView = new TextView(this);
                    spaceView.setText(" ");
                    final TextView spaceView2 = new TextView(this);
                    spaceView2.setText(" ");
                    final TextView spaceView3 = new TextView(this);
                    spaceView3.setText(" ");


                    if (i == 0 || i % 3 == 0) {
                        trainTextView.setText("Training " + ((i /3) + 1) + ":");
                        aantalTextView.setText(waardes[i]);
                        calTextView.setText(" ");
                        calTextView.setPadding(0,-20,0,-20);
                    } else if (i % 3 == 1) {
                        trainTextView.setText("Reps:");
                        aantalTextView.setText(waardes[i]);
                        calorie = Integer.parseInt(waardes[i]);
                        calorie = calorie * Integer.parseInt(waardes[i+1])*8;
                        calTextView.setText("Calorieën verbrand: " + (calorie));
                        calTextView.setPadding(20,20,20,20);
                    } else if (i % 3 == 2) {
                        calTextView.setPadding(0,-20,0,-20);
                        calTextView.setText(" ");
                        trainTextView.setText("Sets:");
                        aantalTextView.setText(waardes[i]);
                    }

                    // Styling van views
                    trainTextView.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    trainTextView.setTextColor((parseColor("#FFFFFF")));

                    aantalTextView.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    aantalTextView.setTextColor((parseColor("#FFFFFF")));

                    calTextView.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    calTextView.setTextColor((parseColor("#FFFFFF")));

                    // Toevoegen van de views aan de layouts
                    dynamic_view_training_aantal.addView(trainTextView);
                    dynamic_view_training.addView(aantalTextView);
                    dynamic_view_calorie.addView(calTextView);

                    if (i > 1 && i % 3 == 2) {
                        dynamic_view_training_aantal.addView(spaceView);
                        dynamic_view_training.addView(spaceView2);
                        dynamic_view_calorie.addView(spaceView3);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.transitie_links_erin, R.anim.transitie_rechts_eruit);
    }

}
