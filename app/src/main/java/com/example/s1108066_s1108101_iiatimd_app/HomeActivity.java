package com.example.s1108066_s1108101_iiatimd_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity{

    String account_naam;
    String account_lftd;
    int account_geslacht;
    int account_geslacht2;
    String account_hdg_gewicht;
    String account_gwn_gewicht;

    private RadioGroup radioGroup;
    private RadioButton radioButton;

    private static final String TRAIN_NAME = "account.txt";

    Button opgeslagen_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_home);

        opgeslagen_button = (Button) findViewById(R.id.opslaan_home_account);
        opgeslagen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toevoegen();
            }
        });

        opgeslagen_button = (Button) findViewById(R.id.reset_home_account);
        opgeslagen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verwijderen();
            }
        });
    }

    //hier pak je de ingevoerde data
    public void toevoegen(){
        EditText Naam = findViewById(R.id.huidige_naam);
        EditText Leeftijd = findViewById(R.id.huidige_leeftijd);
        RadioButton Gender = findViewById(R.id.optie_man);
        RadioButton Gender2 = findViewById(R.id.optie_vrouw);
        EditText Huidige_gewicht = findViewById(R.id.huidige_gewicht);
        EditText Gewenste_gewicht = findViewById(R.id.gewenste_gewicht);

        account_naam = Naam.getText().toString();
        account_lftd = Leeftijd.getText().toString();
        account_hdg_gewicht = Huidige_gewicht.getText().toString();
        account_gwn_gewicht = Gewenste_gewicht.getText().toString();

        save();
    }

    //hier checkt de code of er wat is in gevuld, zo niet geeft die een bericht terug
    public boolean conditie_check( ){
        boolean checker;
        if (!account_naam.isEmpty() && !account_lftd.isEmpty() && !account_gwn_gewicht.isEmpty()){
            checker = true;
        } else {
            checker = false;
        }
        return checker;
    }

    //deze functie saved de ingevoerde waardes
    public void save() {
        if (conditie_check()){
            String text = this.account_naam+ "," +this.account_lftd+ "," +this.account_geslacht+ "," +this.account_geslacht2+ "," +this.account_hdg_gewicht + "," +this.account_gwn_gewicht + ",";
            System.out.println(text);
            FileOutputStream fos = null;
            try {
                fos = openFileOutput("account.txt", MODE_APPEND);
                fos.write(text.getBytes());
                Toast.makeText(this, "gebruiker opgeslagen", Toast.LENGTH_LONG).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            openMainActivity();
        } else {
            Toast.makeText(this, "Vul AUB alles in", Toast.LENGTH_SHORT).show();
        }
    }

    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.transitie_rechts_erin, R.anim.transitie_links_eruit);
    }

    public void verwijderen(){
        FileOutputStream fos;
        try {
            String text = "";
            //Verander hier voor account.txt reset
            fos = openFileOutput(TRAIN_NAME, MODE_PRIVATE);
            fos.write(text.getBytes());
            Toast.makeText(this, "Uw account is verwijderd", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
