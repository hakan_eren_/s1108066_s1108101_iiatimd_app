package com.example.s1108066_s1108101_iiatimd_app;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class TrainingResponse<array> {

    @SerializedName("data")
    public Object data;

    @SerializedName("name")
    public String name;


    public Object getData(){
        return this.data;
    }

    public String getName(){
        return this.name;
    }

    public void setData(Object data){
        this.data = data;
    }

    public void setName(String name){
        this.name = name;
    }

}
