package com.example.s1108066_s1108101_iiatimd_app;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s1108066_s1108101_iiatimd_app.NetworkManager;
import com.example.s1108066_s1108101_iiatimd_app.R;
import com.example.s1108066_s1108101_iiatimd_app.VolleyCallBack;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import static android.graphics.Color.parseColor;

public class ExcerciseActivity extends AppCompatActivity {

    //hier declareren we de properties
    private static final String FILE_NAME = "training.txt";
    private static final String WORKOUT_NAME = "workout.txt";
    public static ArrayList<String> alle_trainingen;
    public static ArrayList<String> eve_workouts = new ArrayList<String>();
    String gkz_training;
    String gkz_reps;
    String gkz_sets;
    AppDatabase db;



    //functie data ophalen wordt aangeroepen wanneer de scherm wordt geladen
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.excercise);

        data_ophalen();



    }

    //hier halen we de data op van de API
    public void data_ophalen(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        alle_trainingen = new ArrayList<String>();
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {

            Spinner dropdown = findViewById(R.id.spinner1);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(ExcerciseActivity.this, android.R.layout.simple_spinner_dropdown_item, alle_trainingen);
            adapter.add(bundle.get("actief").toString());
            dropdown.setAdapter(adapter);

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(ExcerciseActivity.this, android.R.layout.simple_spinner_dropdown_item, alle_trainingen);
        if ( !check_workout_has_data() ) {
            System.out.println("iffer");
            NetworkManager.getInstance(this).getRequest("http://10.0.2.2:8000/api/trainingens", new VolleyCallBack() {
                @Override
                public void onSucces(String result) {
                    String name;
                    String calorie;
                    Training[] trainingen = new Training[12];

                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject obj = jsonArray.getJSONObject(i);
                            name = obj.getString("name");
                            save_workout_to_file(name);

                            trainingen[i] = new Training(name, name, new Random().nextInt(6100000) + 20000);
                            alle_trainingen.add(trainingen[i].getName());
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Spinner dropdown = findViewById(R.id.spinner1);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(ExcerciseActivity.this, android.R.layout.simple_spinner_dropdown_item, alle_trainingen);
                    dropdown.setAdapter(adapter);

                    // DAO DATABASE TESTER
                    AppDatabase db = AppDatabase.getInstance(getApplicationContext());
                    new Thread(new InsertTrainingTask(db, trainingen[0])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[1])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[2])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[3])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[4])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[5])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[6])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[7])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[8])).start();
                    new Thread(new InsertTrainingTask(db, trainingen[9])).start();

                    System.out.println("<---DAO DATABASE--->");
                    System.out.println(db.trainingDAO().getAll().get(0).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(1).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(2).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(3).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(4).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(5).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(6).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(7).getUuid());
                    System.out.println(db.trainingDAO().getAll().get(8).getUuid());

                    System.out.println("<----TrainingenKlassen--->");
                    System.out.println(trainingen[0].getName());
                    System.out.println(trainingen[1].getName());
                    System.out.println(trainingen[2].getName());
                    System.out.println(trainingen[3].getName());
                    System.out.println(trainingen[4].getName());
                    System.out.println(trainingen[5].getName());
                    System.out.println(trainingen[6].getName());
                    System.out.println(trainingen[7].getName());
                    System.out.println(trainingen[8].getName());
                    System.out.println(trainingen[9].getName());
                    // AFLOOP DAO TESTER
                }
            });
        } else {
            System.out.println("effer");
            workout_data_ophalen();

        }
    }

    //hier pak je de ingevoerde
    public void toevoegen(View v){
        Spinner dropdowner = findViewById(R.id.spinner1);
        EditText reps = findViewById(R.id.var_reps);
        EditText sets = findViewById(R.id.var_sets);

        gkz_training = dropdowner.getSelectedItem().toString();
        gkz_reps = reps.getText().toString();
        gkz_sets = sets.getText().toString();
        save(v);
    }

    //deze functie saved de ingevoerde waardes
    public void save(View v) {
        if (conditie_check()){
            String text = this.gkz_training + "," + this.gkz_reps + "," + this.gkz_sets + ",";
            FileOutputStream fos = null;
            try {
                fos = openFileOutput(FILE_NAME, MODE_APPEND);
                fos.write(text.getBytes());
                Toast.makeText(this, "Training Opgeslagen", Toast.LENGTH_LONG).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private boolean check_workout_has_data() {
        boolean has_data = true;
        FileInputStream fis = null;
        try {
            fis = openFileInput(WORKOUT_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder waardess = new StringBuilder();
            String textt;

            while ((textt = br.readLine()) != null) {
                waardess.append(textt).append("\n");
                System.out.print(waardess);
            }
            if (waardess.length() > 0 ){
                has_data = true;
            } else {
                has_data = false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return has_data;
    }

    //deze functie saved de ingevoerde waardes
    public void save_workout_to_file(String name) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(WORKOUT_NAME, MODE_APPEND);
            fos.write(name.getBytes());
            fos.write(",".getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void workout_data_ophalen(){
        FileInputStream fis = null;
        try {
            AppDatabase db = AppDatabase.getInstance(getApplicationContext());

            ArrayList<String> waarde = new ArrayList<String>();

            for(int l = 0; l < 10; l++){
                System.out.println(db.trainingDAO().getAll().get(l).getName());
                if ( db.trainingDAO().getAll().get(l).getName() != null) {
                    waarde.add(db.trainingDAO().getAll().get(l).getName());
                }
            }



            Bundle bundle = getIntent().getExtras();
            System.out.println("boobehtest");
            if(bundle != null) {

                if (bundle.get("actief") != null) {
                    System.out.println("boobeh");
                    Spinner dropdown = findViewById(R.id.spinner1);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(ExcerciseActivity.this, android.R.layout.simple_spinner_dropdown_item, alle_trainingen);
                    System.out.println(bundle.get("actief"));
                    waarde.add(bundle.get("actief").toString());
                    dropdown.setAdapter(adapter);

                }
            }

            Spinner dropdown = findViewById(R.id.spinner1);

            if (waarde != null) {

                ArrayAdapter<String> adapter = new ArrayAdapter<>(ExcerciseActivity.this, android.R.layout.simple_spinner_dropdown_item, waarde);
                System.out.println("boobehlijn84");
                dropdown.setAdapter(adapter);

            }
            System.out.println("boobehlijn84");
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //hier checkt de code of er wat is in gevuld, zo niet geeft die een bericht terug
    public boolean conditie_check( ){
        boolean checker;
        if (!gkz_training.isEmpty() && !gkz_reps.isEmpty() && !gkz_sets.isEmpty()){
            checker = true;
        } else {
            checker = false;
            Toast.makeText(this, "Vul AUB alles in", Toast.LENGTH_SHORT).show();
        }
        return checker;
    }


    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.transitie_links_erin, R.anim.transitie_rechts_eruit);
    }

}